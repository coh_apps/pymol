ARG SOURCE_DOCKER_REGISTRY=localhost:5000

FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu_opt_python:3.8.2-headless AS opt_python

FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu_tools:19.04 AS build

COPY --from=opt_python /opt /opt

ENV PATH /opt/bin/:/usr/bin/:/bin/:/usr/sbin/:/sbin/
ENV PKG_CONFIG_PATH /opt/pkgconfig/
ENV LD_LIBRARY_PATH /opt/lib/

RUN apt install -y libglew-dev libglm-dev libpng-dev libxml2-dev libmsgpack-dev libfreetype6-dev

RUN python -m pip install pyqt5

RUN mkdir -p /tmp/scratch/build && \
    cd /tmp/scratch && wget -t 1 http://localhost:8000/mmtf-cpp-1.0.0.tar.gz || \
                       wget -t 3 https://github.com/rcsb/mmtf-cpp/archive/v1.0.0.tar.gz -O mmtf-cpp-1.0.0.tar.gz && \
    cd /tmp/scratch/ && tar -xvf mmtf-cpp-1.0.0.tar.gz && \
    cd /tmp/scratch/build && cmake -DCMAKE_BUILD_TYPE=Release  \
                                   -Wno-dev -G "Ninja" ../mmtf-cpp-1.0.0/ && \
                             ninja && ninja install && \
    cd / && rm -rf /tmp/scratch

RUN mkdir -p /tmp/scratch/build && \
    cd /tmp/scratch && wget -t 1 http://localhost:8000/PyMOL-2.3.0.tar.gz || \
                       wget -t 3 https://github.com/schrodinger/pymol-open-source/archive/v2.3.0.tar.gz -O PyMOL-2.3.0.tar.gz && \
    cd /tmp/scratch/ && tar -xvf PyMOL-2.3.0.tar.gz && \
    cd /tmp/scratch/pymol-open-source-2.3.0/ && python setup.py install --prefix=/opt && \
    cd / && rm -rf /tmp/scratch

#RUN ls -lart /opt/pymol && false
FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu_base:19.04

COPY --from=build /opt /opt
COPY --from=build /usr/lib/x86_64-linux-gnu/ /opt/lib/

RUN apt install -y libx11-6 libfontconfig1 libxkbcommon-x11-0 libdbus-1-3
RUN apt install -y ca-certificates

ENV PATH /opt/bin/:/usr/bin/:/bin/:/usr/sbin/:/sbin/
ENV PKG_CONFIG_PATH /opt/pkgconfig/
ENV LD_LIBRARY_PATH /opt/lib/

ENTRYPOINT [ "/opt/bin/pymol" ]
